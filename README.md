**Code Editor for the JH Project**

*Installing:*
1. Install MySQL 
2. Run 'npm install' to install all dependencies - this should install all development dependencies too
    - If some dev dependencies are not included for some reason, follow error messages to see which ones and use 'npm install *dependency_name*'
3. Run 'npm run start-dev' to build, watch and run the server - for development
4. Run 'npm run start' to just start the server
5. Default port is 3000

*Documentation on frameworks:*
* [Express](http://expressjs.com/)
* [Jade Templating Engine](http://jade-lang.com/reference/)
* [RethinkDB](https://www.rethinkdb.com/api/javascript/)

Please read report/*.pdf for further info.