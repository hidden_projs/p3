// Module to load config files */
var config = require('./server/config/config');
var randomWords = require('random-words');
var extra = require('./server/miscellaneous/extra_module');
/* App-wide dependencies */
var express = require('express');
var app = module.exports = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

/* Create DB connection */
var mysql = require('mysql');
var dbConnection = mysql.createConnection(config.load('db'));

/* Module that provides gateway to backend implementations */
var backend_module = require('./server/backend/backend_module');
/* Initialise module */
backend_module.init(dbConnection);

/* Initialise session store */
var session = require('express-session');
var MySQLStore = require('express-mysql-session')(session);
var sessionStore = new MySQLStore(config.load('session'), dbConnection);

/* Using the flash middleware provided by connect-flash to store messages in session and displaying in templates */
var flash = require('connect-flash');
app.use(flash());

/* view engine setup */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/* Compresses static content (js, html, css) */
var compression = require('compression');
//noinspection JSCheckFunctionSignatures
app.use(compression());

/* Configure session */
app.use(session({
                    key: 'session_cookie_name',
                    secret: 'session_cookie_secret',
                    store: sessionStore,
                    resave: true,
                    saveUninitialized: true
                }));

/* Initialise security */
var passport = require('passport');
app.use(passport.initialize());
app.use(passport.session());
var initPassport = require('./server/security/init');
initPassport(passport,
             backend_module.serialiseUsr,
             backend_module.deserializeUsr,
             backend_module.login,
             backend_module.register);

/* Module dependencies - internal */
var client_comm = require('./server/client-connection-handler');

/* Define routing */
var routes = require('./server/routes/routing')(passport);
app.use('/', routes);

/* Catch and render 500 error */
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error/error', {
        message: err.message,
        error: err
    });
});

/* catch 404 and forward to error handler */
app.use(function (req, res, next) {
    res.render('error/error404',
               {
                   url: req.url,
                   words: randomWords({
                                          exactly: 200,
                                          join: ' 404 '
                                      })
               });
    // extra.getImage(
    //     randomWords({exactly: 2,join: ' '}), function (img) {
    //     // respond with html page
    //     res.render('error/error404',
    //                {
    //                    url: req.url,
    //                    'img': img
    //                });
    // });
});


/* Server configuration */
var serverPort = 3000;
http.listen(serverPort, function () {
    console.log('listening on *:' + serverPort);
    client_comm.listen(io, dbConnection);
});
