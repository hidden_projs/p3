var backend_connector = require('./backend_connector_local');

module.exports.init = function (params) {
    backend_connector.init(params);
};

module.exports.serialiseUsr = function (params) {
    backend_connector.serialiseUsr(params);
};

module.exports.deserializeUsr = function (params) {
    backend_connector.deserializeUsr(params);
};

module.exports.login = function (params) {
    backend_connector.login(params);
};

module.exports.register = function (params) {
    backend_connector.register(params);
};

module.exports.changePassword = function (params) {
    backend_connector.changePassword(params)
};

module.exports.changeScreenName = function (params) {
    backend_connector.changeScreenName(params)
};

module.exports.createDocument = function (params) {
    backend_connector.createDocument(params);
};

module.exports.getContributors = function (params) {
    backend_connector.getContributors(params);
};
module.exports.getLastModifiedTime = function (params) {
    backend_connector.getLastModifiedTime(params);
};

module.exports.getListOfDocuments = function (params) {
    backend_connector.getListOfDocuments(params);
};

module.exports.getMyUserInfo = function (params) {
    backend_connector.getMyUserInfo(params);
};

module.exports.getPermissions = function (params) {
    backend_connector.getPermissions(params);
};

module.exports.getUserInfo = function (params) {
    backend_connector.getUserInfo(params);
};

module.exports.loadDocument = function (params) {
    backend_connector.loadDocument(params);
};

module.exports.loadLatestDocument = function (params) {
    backend_connector.loadLatestDocument(params);
};

module.exports.loadOldDocument = function (params) {
    backend_connector.loadOldDocument(params);
};


module.exports.reconnect = function (params) {
    backend_connector.reconnect(params);
};

module.exports.sendChange = function (params) {
    backend_connector.sendChange(params);
};

module.exports.sendChatMessage = function (params) {
    backend_connector.sendChatMessage(params);
};

module.exports.setPermissions = function (params) {
    backend_connector.setPermissions(params);
};

module.exports.changeNotification = function (params) {
    backend_connector.changeNotification(params);
};

module.exports.receiveChange = function (params) {
    backend_connector.receiveChange(params);
};
 