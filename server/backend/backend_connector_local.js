var dbConnection;
var mysql = require('mysql');
var bCrypt = require('bcrypt-nodejs');

module.exports.init = function (params) {
    dbConnection = params;
};

module.exports.serialiseUsr = function (params) {
    params.callback(null, params.user);
};

module.exports.deserializeUsr = function (params) {
    var sql = sqlr("CALL getuserbyid(?)", params.id);

    dbConnection.query(sql, function (err, rs) {
                           if (err) {
                               params.callback('message', sqle(err), null);
                           } else {
                               var result = sqlx(rs);
                               if (result.errcode != '00000') params.callback('message', sqlee(result), null);
                               params.callback(null, result);
                           }
                       }
    );
};

module.exports.login = function (params) {
    var sql = sqlr("CALL login(?)", [params.username]);

    dbConnection.query(sql, function (err, rs) {
                           if (err) {
                               return params.callback(null, false, params.req.flash('message', sqle(err)));
                           } else {
                               var result = sqlx(rs);
                               if (result.errcode != '00000') return params.callback(null,
                                                                                     false,
                                                                                     params.req.flash('message', sqlee(result)));
                               if (!isValidPassword(result, params.password)) {
                                   return params.callback(null,
                                                          false,
                                                          params.req.flash('message', 'Try Harder'));
                               }
                               return params.callback(null, result);
                           }
                       }
    );
};

module.exports.register = function (params) {
    var findOrCreateUser = function () {
        var sql = sqlr("CALL createuser(?, ?, ?)", [params.username, createHash(params.password), params.nickname]);

        dbConnection.query(sql, function (err, rs) {
                               if (err) {
                                   return params.callback(null, false, params.req.flash('message', sqle(err)));
                               } else {
                                   var result = sqlx(rs);
                                   if (result.errcode != '00000') return params.callback(null,
                                                                                         false,
                                                                                         params.req.flash('message', sqlee(result)));
                                   return params.callback(null, result);
                               }
                           }
        );
    };
    // Delay the execution of findOrCreateUser and execute the method
    // in the next tick of the event loop
    process.nextTick(findOrCreateUser);
};

var isValidPassword = function (user, password) {
    return bCrypt.compareSync(password, String.fromCharCode.apply(null, user.password));
};

var createHash = function (password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};


var sqlx = function (x) {
    return x.filter(function (n) {
        return !((n.constructor === Array) && (n.length < 1) && (n != undefined));
    })[0][0];
};

var sqle = function (x) {
    return 'SQL code: ' + x.errno + ', ' + x.code;
};

var sqlee = function (x) {
    return 'SQL code: ' + x.errcode + ', ' + x.errmsg;
};

var sqlr = function (sql, params) {
    var r = mysql.format(sql, params);
    console.log(r);
    return r;
};


var dbCall = function (proc, params, cb) {
    try {
        var sql = sqlr("CALL " + proc + "(" + getQuestions(params.length) + ")", params);
        dbConnection.query(sql, function (err, rs) {
                               if (err) {
                                   cb(sqle(err));
                               } else {
                                   var result = sqlx(rs);
                                   if (result.errcode != '00000') cb(sqlee(result));
                                   cb(null, result);
                               }
                           }
        );
    } catch (e) {
        cb('Not Implemented ' + e);
    }
};

var getQuestions = function (size) {
    var x = [];
    for (i = 0; i < size; i++) {
        x.push('?');
    }
    return x.join(", ");
};


module.exports.createDocument = function (params) {
    dbCall('createfile', [params.userid, params.filename, params.filetype], params.callback);
};
module.exports.getListOfDocuments = function (params) {
    dbCall('getlistoffiles', [params.id], params.callback);
};
module.exports.changePassword = function (params) {
    dbCall("changepassword", [params.userid, params.newpassword,], params.callback);
};
module.exports.changeScreenName = function (params) {
    dbCall("changescreenname", [params.userid, params.newnickname], params.callback);
};
module.exports.getContributors = function (params) {
    dbCall("getcontributors", [params.fileid], params.callback);
};
module.exports.getLastModifiedTime = function (params) {
    dbCall("getlastmodifiedtime", [params.fileid], params.callback);
};
module.exports.getMyUserInfo = function (params) {
    dbCall("getmyuserinfo", [params.userid], params.callback);
};
module.exports.getPermissions = function (params) {
    dbCall("getpermissions", [params.userid], params.callback);
};
module.exports.getUserInfo = function (params) {
    dbCall("getuserinfo", [params.username], params.callback);
};
module.exports.loadDocument = function (params) {
    dbCall("loaddocument", [params.fileid], params.callback);
};
module.exports.loadLatestDocument = function (params) {
    dbCall("loadlatestdocument", [params.fileid], params.callback);
};
module.exports.loadOldDocument = function (params) {
    dbCall("loadolddocument", [params.fileid], params.callback);
};
module.exports.reconnect = function (params) {
    dbCall("reconnect", [params.call], params.callback);
};
module.exports.sendchange = function (params) {
    dbCall("sendChange", [params.change, params.fileid, params.linenumber], params.callback);
};
module.exports.sendchatmessage = function (params) {
    dbCall("sendChatMessage", [params.msg, params.touserid, params.fromuserid], params.callback);
};
module.exports.setpermissions = function (params) {
    dbCall("setPermissions", [params.fileid, params.perms], params.callback);
};
module.exports.changenotification = function (params) {
    dbCall("changeNotification", [params.changenot], params.callback);
};
module.exports.receivechange = function (params) {
    dbCall("receiveChange", [params.receivedchange], params.callback);
};