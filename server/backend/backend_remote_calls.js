var WebSocket = require('ws');
var callback_list = [{}];

var server1 = "ws://bc37.host.cs.st-andrews.ac.uk:10000/server/";
var server2 = "ws://fare.scot:10000";

function getServer() {
    return server2;
}

module.exports = {
    "deserializeUsr": "deserializeUsr",
    "serialiseUsr": "serialiseUsr",
    "register": "register",
    "login": "login",
    "reconnect": "reconnect",
    "createDocument": "createDocument",
    "loadDocument": "loadDocument",
    "loadOldDocument": "loadOldDocument",
    "loadLatestDocument": "loadLatestDocument",
    "sendChange": "sendChange",
    "receiveChange": "receiveChange",
    "changeNotification": "changeNotification",
    "getContributors": "getContributors",
    "getPermissions": "getPermissions",
    "setPermissions": "setPermissions",
    "getLastModifiedTime": "getLastModifiedTime",
    "changeScreenName": "changeScreenName",
    "changePassword": "changePassword",
    "sendChatMessage": "sendChatMessage",
    "getListOfDocuments": "getListOfDocuments",
    "getUserInfo": "getUserInfo",
    "getMyUserInfo": "getMyUserInfo"
};

var init = function (properties) {
};

var deserializeUsr = function (params) {
    console.log('deserializing user:', params.userId);
    getUserInfo({user_id: params});
};

var serialiseUsr = function (user, done) {
    console.log('serializing user: ');
    console.log(user);
    done(null, user.user_id);
};
//noinspection JSCommentMatchesSignature
/**
 * Calls backend action register.
 * @param username username
 * @param password password
 * @param screen_name user nickname
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object: {
 *      'keyword': 'REGISTERED'
 *      }
 */
var register = function (parameters) {
    var username = parameters.username;
    var password = parameters.password;
    var screen_name = parameters.screen_name;
    var request_id = guid();
    var callback = parameters.callback;
    var ws = new WebSocket(getServer);
    var request = {
        "keyword": "REGISTER",
        "username": username,
        "password": password,
        "screen_name": screen_name,
        "request_id": request_id
    };
    ws.on('open', function () {
        ws.send(request);
    });
    ws.on('message', function (message) {
        callback(message, ws);
    });
    ws.open();
};

//noinspection JSCommentMatchesSignature
/**
 * Calls backend action login.
 *
 * @param username username
 * @param password password
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object: {
 *      "keyword": "OK_AUTHENTICATED",
 *      "usr_otp": "one_time_pass",
 *      "request_id": "request_id"
 *      }
 */
var login = function (parameters) {
    var username = parameters.username;
    var password = parameters.password;
    var request_id = guid();
    var callback = parameters.callback;
    var ws = new WebSocket(getServer);
    var request = {
        "keyword": "AUTHENTICATE",
        "username": username,
        "password": password,
        "request_id": request_id
    };
    ws.on('open', function () {
        ws.send(request);
    });
    ws.on('message', function (message) {
        callback(message, ws);
    });
    ws.open();
};

//noinspection JSCommentMatchesSignature
/**
 * Calls backend action reconnect.
 *
 * @param username username
 * @param password password
 * @param usr_otp usr_otp
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object: {
 *      "keyword": "OK_AUTHENTICATED",
 *      "usr_otp": "new_one_time_pass",
 *      "request_id": "request_id"
 *      }
 */
var reconnect = function (parameters) {
    var ws = parameters.ws;
    var username = parameters.username;
    var usr_otp = parameters.usr_otp;
    var request_id = guid();
    var callback = parameters.callback;
    var request = {
        "keyword": "RECONNECT",
        "username": username,
        "usr_otp": usr_otp,
        "request_id": request_id
    };
    callback_list.push(request_id, callback);
    ws.send(request);
};

//noinspection JSCommentMatchesSignature
/**
 *
 * Calls backend action create document.
 *
 * @param document_name document_name
 * @param file_type Some identifier for your frontend's file format (e.g. "text", "image")
 * @param data_type "text" for normal textual data. "bin" for binary data (NOT AVAILABLE YET)
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object: {
 * "keyword": "OK_DOC_LATEST",
 *       "document_uuid": "doc_uuid",
 *       "ver_maj": "ver_maj", // latest major version number
 *       "ver_min": "ver_min", // ver_min = latest minor version number
 *       "document_content": "doc_content",
 *       "request_id": "request_id"
 *      }
 */
var createDocument = function (parameters) {
    var ws = parameters.ws;
    var document_name = parameters.document_name;
    var file_type = parameters.file_type;
    var data_type = parameters.data_type;
    var request_id = guid();
    var callback = parameters.callback;
    var request = {
        "keyword": "CREATE",
        "document_name": document_name,
        "file_type": file_type,
        "data_type": data_type,
        "request_id": request_id
    };
    callback_list.push(request_id, callback);
    ws.send(request);
};

//noinspection JSCommentMatchesSignature
/**
 *
 * Calls backend action load document.
 *
 * @param document_uuid document_uuid
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object: {
 *      "keyword": "OK_DOC_LATEST",
 *      "ver_maj": "ver_maj", // latest major version number
 *      "ver_min": "ver_min", // ver_min = latest minor version number
 *      "document_content": "doc_content",
 *      "request_id": "request_id"
 *   }
 */
var loadDocument = function (parameters) {
    var ws = parameters.ws;
    var document_uuid = parameters.document_uuid;
    var request_id = guid();
    var callback = parameters.callback;
    var request = {
        "keyword": "LOAD",
        "document_uuid": document_uuid,
        "request_id": request_id
    };
    callback_list.push(request_id, callback);
    ws.send(request);
};

//noinspection JSCommentMatchesSignature
/**
 *
 * Calls backend action load old document.
 *
 * @param ver_maj ver_maj
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object: {
 * "keyword": "OK_DOC_VERSION",
 * "ver_maj": "ver_maj",
 * "doc_content": "doc_content",
 * "request_id": "request_id"
 * }
 */
var loadOldDocument = function (parameters) {
    var ws = parameters.ws;
    var ver_maj = parameters.ver_maj;
    var request_id = guid();
    var callback = parameters.callback;
    var request = {
        "keyword": "GET_DOC_VERSION",
        "ver_maj": ver_maj,
        "request_id": request_id
    };
    callback_list.push(request_id, callback);
    ws.send(request);
};

//noinspection JSCommentMatchesSignature,JSCommentMatchesSignature
/**
 *
 * Calls backend action load latest document.
 * <p>
 *
 * This response is the same one that is sent by the server during the initial handshake.
 * This could be useful for re-syncing document data if there is some kind of problem.
 *
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object: {
 * "keyword": "OK_DOC_LATEST",
 * "ver_maj": "ver_maj",
 * "ver_min": "ver_min",
 * "document_content": "content",
 * "request_id": "request_id"
 * }
 */
var loadLatestDocument = function (parameters) {
    var ws = parameters.ws;
    var request_id = guid();
    var callback = parameters.callback;
    var request = {
        "keyword": "GET_DOC_LATEST",
        "request_id": request_id
    };
    callback_list.push(request_id, callback);
    ws.send(request);
};

//noinspection JSCommentMatchesSignature
/**
 *
 * Calls backend action send change.
 *
 * @param ver_maj Target major version number.
 * Essentially this is the current version that the client has and is applying the change against.
 * If the server is ahead in versions then this is used to resolve conflicts in the position of the change.
 * @param ver_min target minor version number. As above.
 * @param change_type  "0" for addition, "1" for removal
 * @param position the number of characters into the document that the change occurs.
 * @param content the actual data being added for addition changes,
 * or just the number of characters being removed for removal changes
 * @param remaining_parts boolean set this to false for a normal singular change.
 * Setting this to true will queue the change up but not actually apply it.
 * This is useful if there are multiple diffs that make up a single atomic change.
 * For example, replacing some block of text with another might continue
 * a removal followed by an addition change.
 * Because of autosaving etc. there could be a version of the document
 * stored with the removal but not the addition.
 * By chaining them together, they can be applied atomically.
 * Any number of changes can be queued, and as soon as a change with this value set to 'false' is received,
 * all items in the queue are applied in order, followed by the change just received.
 * Server can respond in one of two ways:
 *
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object1:  {
 * "keyword": "OK_CHANGE_QUEUED",
 * "queue_size": "queue_size",
 * "request_id": "request_id"
 * }
 *
 * Response object2: {
 * "keyword": "OK_CHANGE_ACCEPTED",
 * "ver_maj": "ver_maj",
 * "ver_min": "ver_min",
 * "request_id": "request_id"
 * }
 */
var sendChange = function (parameters) {
    var ws = parameters.ws;
    var ver_maj = parameters.ver_maj;
    var ver_min = parameters.ver_min;
    var change_type = parameters.change_type;
    var position = parameters.position;
    var content = parameters.content;
    var remaining_parts = parameters.remaining_parts;
    var request_id = guid();
    var callback = parameters.callback;
    var request = {
        "keyword": "SEND_CHANGE",
        "ver_maj": ver_maj,
        "ver_min": ver_min,
        "change_type": change_type,
        "position": position,
        "content": content,
        "remaining_parts": remaining_parts,
        "request_id": request_id
    };
    callback_list.push(request_id, callback);
    ws.send(request);
};

//noinspection JSCommentMatchesSignature
/**
 *
 * Calls backend action receive change.
 *
 * <p>
 * Changes are sent automatically by the server to every other user connected
 * that is also on the same document.
 * As opposed to the request sent by the client,
 * the version numbers are the actual version numbers associated with this change.
 *
 * @param change a change object?
 * @param callback function to be called on server reply
 *
 * Response object: 'keyword': 'REGISTERED'
 */
var receiveChange = function (parameters) {
    var ws = parameters.ws;
    var change = parameters.change;
    var callback = parameters.callback;
    //TODO: how to use that????????
    var response = {
        "keyword": "CHANGE",
        "change_type": "change_type",
        "ver_maj": "ver_maj",
        "ver_min": "ver_min",
        "position": "position",
        "content": "content",
        "request_id": "request_id"
    }
};

//noinspection JSCommentMatchesSignature
/**
 *
 * Calls backend action change notification.
 *
 * <p>
 * This happens when an autosave takes place,
 * the major version number rolls up by one,
 * and the minor version number resets to zero (as there are no more pending changes).
 * Clients should make sure they take note of the new version and use this for sending change requests etc.
 * Otherwise, no further action is needed.
 *
 * @param ver_maj ver_maj
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object: 'keyword': 'REGISTERED'
 */
var changeNotification = function (parameters) {
    var ws = parameters.ws;
    var ver_maj = parameters.ver_maj;
    var request_id = guid();
    var callback = parameters.callback;
    var response = {
        "keyword": "NOTICE_NEW_VERSION",
        "ver_maj": ver_maj,
        "request_id": request_id
    };
    //TODO: how to use this??????????
};

/**
 *
 * Calls backend action get contributors.
 *
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object: {
 * "keyword": "OK_DOC_USERS",
 * "users": ["user1","user2",...],
 * "request_id": "request_id"
 * }
 */
var getContributors = function (parameters) {
    var request_id = guid();
    var callback = parameters.callback;
    var ws = parameters.ws;
    var request = {
        "keyword": "GET_DOC_USERS",
        "request_id": request_id
    };
    callback_list.push(request_id, callback);
    ws.send(request);
};

/**
 *
 * Calls backend action get permissions.
 *
 * @param user user
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object: {
 * "keyword": "OK_PERMISSIONS",
 * "user": "user",
 * "permission": "permission",
 * "request_id": "request_id"
 * }
 *
 * The permission is just a string that represents the privilege level of the given user.
 * Here are some sample permissions:
 *
 * Permission Token:Permissions
 *
 * rwo:Read, write, owner (can modify permissions)
 * rw:Read, write
 * r:Read
 * none:No permissions
 *
 * These are enforced automatically by the server.
 * The document creator is given 'rwo' permissions initially.
 * They can then set permissions for other users to allow them to read the document etc.
 */
var getPermissions = function (user, request_id, callback) {
    var ws = parameters.ws;
    var request = {
        "keyword": "GET_PERMISSIONS",
        "user": user,
        "request_id": request_id
    };
    callback_list.push(request_id, callback);
    ws.send(request);
};

/**
 *
 * Calls backend action set permissions.
 *
 * @param user user
 * @param permissions permissions
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object: {
 * "keyword": "OK_PERMISSIONS_SET",
 * "user": "user",
 * "request_id": "request_id"
 * }
 *
 * For permissions look at getPermissions()
 */
var setPermissions = function (user, permissions, request_id, callback) {
    var ws = parameters.ws;
    var request = {
        "keyword": "SET_PERMISSIONS",
        "user": user,
        "permissions": permissions,
        "request_id": request_id
    };
    callback_list.push(request_id, callback);
    ws.send(request);
};

/**
 *
 * Calls backend action login.
 *
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object: {
 * "keyword": "OK_LAST_MODIFIED",
 * "timestamp": "timestamp",
 * "request_id": "request_id"
 * }
 */
var getLastModifiedTime = function (request_id, callback) {
    var ws = parameters.ws;
    var request = {
        "keyword": "GET_LAST_MODIFIED",
        "request_id": request_id
    };
    callback_list.push(request_id, callback);
    ws.send(request);
};

/**
 *
 * Calls backend action change screen name.
 *
 * @param screen_name user nickname
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object: {
 * "keyword": "OK_SCREEN_NAME_CHANGED",
 * "request_id": "request_id"
 * }
 */
var changeScreenName = function (screen_name, request_id, callback) {
    var ws = parameters.ws;
    var request = {
        "keyword": "RENAME",
        "new_screen_name": screen_name,
        "request_id": request_id
    };
    callback_list.push(request_id, callback);
    ws.send(request);
};

/**
 *
 * Calls backend action change password.
 *
 * @param old_pass old password
 * @param new_pass new password
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object: {
 * "keyword": "OK_PASSWORD_CHANGED",
 * "request_id": "request_id"
 * }
 */
var changePassword = function (old_pass, new_pass, request_id, callback) {
    var ws = parameters.ws;
    var request = {
        "keyword": "PASSWORD_CHANGE",
        "old_password": old_pass,
        "new_password": new_pass,
        "request_id": request_id
    };
    callback_list.push(request_id, callback);
    ws.send(request);
};

/**
 *
 * Calls backend action send chat message.
 *
 * @param message message
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object: {
 * "keyword": "CHAT",
 * "username": "username",
 * "message": "message",
 * "request_id": "request_id"
 * } // including to sender!
 */
var sendChatMessage = function (message, request_id, callback) {
    var ws = parameters.ws;
    var request = {
        "keyword": "SEND_CHAT",
        "message": message,
        "request_id": request_id
    };
    callback_list.push(request_id, callback);
    ws.send(request);
};

/**
 *
 * Calls backend action get list of documents.
 *
 * @param withMetadata boolean that specifies reply message to add or not to add metadata
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object: Response object without meta: {
 * "keyword": "OK_DOCUMENT_LIST",
 * "document_list": [{
 * "uuid": "uuid",
 * "name": "doc_name",
 * "owner": "owner",
 * "last_modified": "timestamp",
 * "type": "type",
 * "size": "size"
 * }],
 * "request_id": "request_id"
 * }
 *
 *
 * Response object with meta: {
 * "keyword": "OK_DOCUMENT_LIST",
 * "document_list": [{
 * "uuid": "uuid",
 * "name": "doc_name"
 * }],
 * "request_id": "request_id"
 * }
 *
 */
var getListOfDocuments = function (withMetadata, request_id, callback) {
    var ws = parameters.ws;
    var request = {
        "keyword": "GET_DOCUMENT_LIST",
        "get_metadata": withMetadata,
        "request_id": request_id
    };
    callback_list.push(request_id, callback);
    ws.send(request);
};

//noinspection JSCommentMatchesSignature
/**
 *
 * Calls backend action get user info.
 *
 * @param user_id unique user id
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object:  {
 * "keyword": "OK_USER_DETAILS",
 * "user_id": "user_id",
 * "screen_name": "screen_name",
 * "num_doc": 0,
 * "request_id": "request_id"
 * }
 */
var getUserInfo = function (parameters) {
    var ws = parameters.ws;
    var user_id = parameters.user_id;
    var request_id = guid();
    var callback = parameters.callback;
    var request = {
        "keyword": "GET_USER_DETAILS",
        "user_id": user_id,
        "request_id": request_id
    };
    callback_list.push(request_id, callback);
    ws.send(request);
};

//noinspection JSCommentMatchesSignature
/**
 *
 * Calls backend action get my user info.
 *
 * @param request_id request_id
 * @param callback function to be called on server reply
 *
 * Response object:  {
 * "keyword": "OK_PERSONAL_DETAILS",
 * "user_id": "user_id",
 * "screen_name": "screen_name",
 * "num_docs": 0,
 * "request_id": "request_id"
 * }
 */
var getMyUserInfo = function (parameters) {
    var ws = parameters.ws;
    var request_id = guid();
    var callback = parameters.callback;
    var request = {
        "keyword": "GET_PERSONAL_DETAILS",
        "request_id": request_id
    };
    ws.on('message', function (message) {
        callback(message);
    });
};

var parseSocketMessage = function (message) {

};

/**
 * Generates GUIDs for request_idS
 * @returns {string} GUID
 */
function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    var x = (s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4()).toString();
    return x;
}