var dbConnection;
var session;
var backend_calls = require('./backend_remote_calls');

module.exports.init = function (dbConn, sess) {
    dbConnection = dbConn;
    session = sess;
};

module.exports.serialiseUsr = function (user, done) {
    console.log('serializing user: ', user);
    done(null, user.id);
};

module.exports.deserializeUsr = function (userId, done) {
    console.log('deserializing user:', userId);
    module.exports.getUserInfo(userId, 'requestId', function (message) {
        var user = {
            "id": message.user_id,
            "nickname": message.screen_name,
            "num_doc": message.num_doc
        }
        done(err, user);
    })
};

module.exports.login = function (req, username, password, done) {
    console.log('Login for  user: ', username);
    backend_calls.login({
                            req: username,
                            username: password,
                            password: '',
                            done: function (message) {
                                if (message.keyword == 'OK_AUTHENTICATED') {
                                    backend_calls.getMyUserInfo(username, password, '', function (message) {
                                        var user = {
                                            "id": message.user_id,
                                            "nickname": message.screen_name,
                                            "username": username,
                                            "password": password,
                                        }
                                        return done(null, user);
                                    })
                                } else {
                                    console.log('Error in Login: ' + message);
                                    return done(message);
                                }
                            }
                        });
};

module.exports.register = function (req, username, password, done) {
    var findOrCreateUser = function () {
        console.log('Register for  user: ', username);

        backend_calls.register({
                                   req: username,
                                   username: password,
                                   password: req.param('nickname'),
                                   done: ''
                               }, function (message) {
            if (message.keyword == 'REGISTERED') {
                backend_calls.getMyUserInfo(username, password, '', function (message) {
                    var user = {
                        "id": message.user_id,
                        "nickname": req.param('nickname'),
                        "username": username,
                        "password": password
                    };
                    console.log('User Registration successful: ' + user);
                    return done(null, user);
                });
            } else {
                console.log('Error in Register: ' + message);
                return done(message);
            }
        });
    };
    // Delay the execution of findOrCreateUser and execute the method
    // in the next tick of the event loop
    process.nextTick(findOrCreateUser);
};
