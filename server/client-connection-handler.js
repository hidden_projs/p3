/* jshint loopfunc: true */
"use strict";

/* Connection protocol literals
 * Refactor?
 */
const CLIENT_CONNECTION = 'connection';
const CLIENT_DISCONNECTION = 'disconnect';
const CLIENT_FILE_CHANGE = 'editor_change';
const CLIENT_REQUEST_FILE_LISTING = 'list_files';
const CLIENT_OPEN_FILE = 'open_file';

/* Message tags for messages sent to client */
const FILE_TO_CLIENT = 'file_content';
const CHANGE_TO_CLIENT = 'file_change';

var ClientCommunicationHandler = {};
var connectedClients = [];
var io;

/* Handles a user disconnecting, removing them from the list of active editors of a file. */
function handleDisconnect(socket) {
    var index = connectedClients.indexOf(socket);
    connectedClients.splice(index, 1);
}

/* Adds a client (their socket) to the list of active users editing given file */
function addClientToListOfActiveClients(socket) {
    connectedClients.push(socket);
}

/* Takes a socket out of all rooms it's currently in */
function leaveAllRooms(socket) {
    var allRooms = socket.rooms;
    for (let room of allRooms) {
        // Stay in own room.
        if (room == socket.id) continue;

        socket.leave(room, function (err) {
            console.log("Error leaving room, socket ID: " + socket.id + ", tried to leave room: " + room);
        });
    }
}

/* Joins the given socket to the given room */
function joinRoom(roomName, socket) {
    socket.join(roomName, function (err) {
        if (err) {
            console.log("Socket ID: " + socket.id + ", cound not join room: " + roomName);
        } else {
            console.log("Socket ID: " + socket.id + ", joined room: " + roomName + " successfully.");
        }
    });
}

/* Attempt to make a change to a file */
function fileChangeRequest(socket, message) {
    //TODO - check with DB if change is ok?

    socket.to(message.fileId).emit(CHANGE_TO_CLIENT, message);
}

function sendFileToUser(socket, fileId) {
    // TODO - Grab file from DB, send it to user.

    // Mock content
    var file = {};
    file.title = 'Mock file - remove from code';
    file.content = 'function (x, y, z) { return x + y + z; }';
    socket.emit(FILE_TO_CLIENT, file);
}

/* Handles listening for incoming data on websockets */
ClientCommunicationHandler.listen = function listen(socketio, dbConnection) {
    io = socketio;
    io.on(CLIENT_CONNECTION, function (socket) {
        console.log('Websocket connection initiated. ID: ' + socket.id);
        addClientToListOfActiveClients(socket);

        socket.on(CLIENT_OPEN_FILE, function (message) {
            joinRoom(message.fileId, socket);
            sendFileToUser(socket, message);
        });

        socket.on(CLIENT_DISCONNECTION, function () {
            console.log('Websocket disconnected.');
            handleDisconnect(socket);
        });

        socket.on(CLIENT_FILE_CHANGE, function (message) {
            fileChangeRequest(socket, message);
            console.log(message);
        });

        socket.on(CLIENT_FILE_CHANGE, function (message) {
            fileChangeRequest(socket, message);
        });

        socket.on("editor_code_send", function (code, lan) {
            var fs = require('fs');
            const exec = require('child_process').exec;
            switch (lan) {
                case "Python":
                    fs.writeFile("code.py", code, function (err) {
                        exec('python code.py', (error, stdout, stderr) => {
                            socket.emit('editor_code_receive', `${stdout}`);
                            socket.emit('editor_code_receive_error', `${stderr}`);
                        });
                    });
                    break;
                case "Haskell":
                    fs.writeFile("code.hs", code, function (err) {
                        exec('ghc code.hs', (error, stdout, stderr) => {
                            socket.emit('editor_code_receive_error', `${stderr}`);
                            exec('./code', (error, stdout, stderr) => {
                                socket.emit('editor_code_receive', `${stdout}`);
                            });
                        });
                    });
                    break;

                case "Java":
                    fs.writeFile("code.java", code, function (err) {
                        exec('javac code.java', (error, stdout, stderr) => {
                            socket.emit('editor_code_receive_error', `${stderr}`);
                            exec('java Code', (error, stdout, stderr) => {
                                console.log(`${stdout}`);
                                socket.emit('editor_code_receive', `${stdout}`);
                            });
                        });
                    });
                    break;

                case "Javascript":
                    fs.writeFile("code.js", code, function (err) {
                        exec('node code.js', (error, stdout, stderr) => {
                            socket.emit('editor_code_receive', `${stdout}`);
                            socket.emit('editor_code_receive_error', `${stderr}`);
                        });
                    });
                    break;

            }
        });

        socket.on("debugsql", function (message) {
            dbConnection.query(message, function (err, rs) {
                if (err) {
                    socket.emit('debugsql', 'ooups\n' + JSON.stringify(err));
                } else {
                    socket.emit('debugsql', JSON.stringify(rs));
                }
            });
        });
    });
};

module.exports = ClientCommunicationHandler;

