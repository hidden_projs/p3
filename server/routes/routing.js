var express = require('express');
var router = express.Router();
var path = require('path');

var isAuthenticated = function (req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler 
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects
    if (req.isAuthenticated())
        return next();
    // if the user is not authenticated then redirect him to the login page
    res.redirect('/');
};

module.exports = function (passport) {
    /* Handle Logout */
    router.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    /* Handle Login POST */
    router.post('/login', passport.authenticate('login', {
        successRedirect: '/home',
        failureRedirect: '/',
        failureFlash: true
    }));

    /* Handle Registration POST */
    router.post('/register', passport.authenticate('signup', {
        successRedirect: '/home',
        failureRedirect: '/register',
        failureFlash: true
    }));


    /* User Profile */
    router.get('/profile', function (req, res) {
        res.render(path.join('pagescontent', 'profile'), {
                       title: 'Profile'
                   }
        );
    });

    /* GET login page. */
    router.get('/', function (req, res) {
        // Display the Login page with any flash message, if any
        res.render(path.join('auth', 'login'), {
            title: 'Login',
            message: req.flash('message')
        });
    });

    /* GET Registration Page */
    router.get('/register', function (req, res) {
        res.render(path.join('auth', 'register'), {
            title: 'Register',
            message: req.flash('message')
        });
    });

    /* Open editor */
    router.get('/edit/:fileId', isAuthenticated, function (req, res) {
        var file = {}; //dbConnection.getFile(req.params.fileId);
        // TODO
        file.name = "Emil";
        file.content = "function (x)";
        file.fileId = '1';
        res.render(path.join('pagescontent', 'editor'),
                   {
                       title: 'Editor',
                       fileName: file.name,
                       fileContent: file.content,
                       fileId: file.fileId
                   }
        );
    });

    /* GET Home Page */
    router.get('/home', isAuthenticated, function (req, res) {
        res.render(path.join('pagescontent', 'filelisting'), {
            title: "Files",
            user: req.user
        });
    });

    /* GET Debug SQL Page */
    router.get('/debug', function (req, res) {
        res.render(path.join('pagescontent', 'debugsql'), {
            title: 'Debug SQL'
        });
    });
    return router;
};


