var login = require('./login');
var register = require('./register');

module.exports = function (passport, serialiseUsr, deserialiseUsr, lgn, rgstr) {
    // Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function (user, done) {
        done(null, user.id)
    });

    passport.deserializeUser(function (id, done) {
        deserialiseUsr({
                           "id": id,
                           "callback": done
                       });
    });

    // Setting up Passport Strategies for Login and SignUp/Registration
    login(passport, lgn);
    register(passport, rgstr);
}