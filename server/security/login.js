var LocalStrategy = require('passport-local').Strategy;

module.exports = function (passport, login) {
    passport.use('login', new LocalStrategy({
            passReqToCallback: true
        },
        function (req, username, password, done) {
            login({
                      "req": req,
                      "username": username,
                      "password": password,
                      "callback": done
                  });
        }));
};