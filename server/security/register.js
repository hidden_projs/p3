var LocalStrategy = require('passport-local').Strategy;

module.exports = function (passport, register) {
    passport.use('signup', new LocalStrategy({
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function (req, username, password, done) {
            register({
                         "req": req,
                         "username": username,
                         "password": password,
                         "nickname": username,
                         "callback": done
                     });
        })
    );
};