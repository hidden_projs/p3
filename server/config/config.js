var fs = require('fs');
var properties = require('properties');

var options = {
    namespaces: true,
    sections: true,
    comments: "#",
    separators: "=",
    strict: true
};

/**
 * Function allows to load and parse .ini files into js objects.
 * @param filename name of the properties.ini file, without extension and located in ./config/ folder.
 * @returns object generated from properties file.
 */
module.exports.load = function (filename) {
    var fileAsString = fs.readFileSync("./server/config/" + filename + ".ini").toString();
    // noinspection ES6ModulesDependencies
    // console.log('loaded ' + filename + ' properties:');
    var obj = properties.parse(fileAsString, options);
    // noinspection ES6ModulesDependencies
    // console.log('parsed ' + filename + ' properties:');
    // noinspection ES6ModulesDependencies
    // console.log(obj);
    return obj;
};
