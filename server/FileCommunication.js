/* Interfaces connections from client to establish connections, edit and close files. */

"use strict";

var fileComm = class FileCommunicationHandler {
    /* Attempts to open a connection to file for current user. 
     * Returns true if successful, false if not. */
    openRemoteFile(fileUid) {
       // TODO - Use DBConnection module check permission of file 
    }

    /* Applies change delta to currently opened file.
     * Returns false if it failes or if no file is currently open. */
    changeRemoteFile(deltaChanges) {
        // TODO - Use DBConnection to submit changes
    }

    /* Closes currently opened file */
    closeRemoteFile(fileUid) {
        // TODO - Use DBConnection to close a file
    }

    /* Handles data coming from client. */
    handleDataFromClient(data) {
        // TODO
    }

    /* Returns a message to be sent to the client, based on data retrieved from server. */
    constructMessageForClient(dataFromServer) {
        // TODO - Handle incoming data
    }
};

module.exports = fileComm;
